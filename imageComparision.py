from skimage import metrics
import argparse
import imutils
import cv2

# load the two input images
imageA = cv2.imread("Q_Emu.png")
imageB = cv2.imread("Q_Galaxy.png")

# Match size of both images
# Get height and width of imageA
height = int(imageA.shape[0])
width = int(imageA.shape[1])
print(imageB.shape)
dim = (width, height)

# Resize imageB with image A dimensions
resizedImageB = cv2.resize(imageB, dim, interpolation=cv2.INTER_AREA)
print('Resized Dimensions : ', resizedImageB.shape)
print('ImageA Dimensions : ', imageA.shape)

# convert the images to grayscale
grayA = cv2.cvtColor(imageA, cv2.COLOR_BGR2GRAY)
grayB = cv2.cvtColor(resizedImageB, cv2.COLOR_BGR2GRAY)

# compute the Structural Similarity Index (SSIM) between the two
# images, ensuring that the difference image is returned
(score, diff) = metrics.structural_similarity(grayA, grayB, full=True)
diff = (diff * 255).astype("uint8")
print("SSIM: {}".format(score))

# threshold the difference image, followed by finding contours to
# obtain the regions of the two input images that differ
thresh = cv2.threshold(diff, 0, 255,
                       cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                        cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)

# loop over the contours
for c in cnts:
    # compute the bounding box of the contour and then draw the
    # bounding box on both input images to represent where the two
    # images differ
    (x, y, w, h) = cv2.boundingRect(c)
    cv2.rectangle(grayA, (x, y), (x + w, y + h), (0, 0, 255), 2)
    cv2.rectangle(grayB, (x, y), (x + w, y + h), (0, 0, 255), 2)
# show the output images
cv2.imshow("Original", grayA)
cv2.imshow("Modified", grayB)
cv2.imshow("Diff", diff)
cv2.imshow("Thresh", thresh)
cv2.waitKey(0)
